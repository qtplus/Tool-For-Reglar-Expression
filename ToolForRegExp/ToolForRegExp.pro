#-------------------------------------------------
#
# Project created by QtCreator 2018-03-23T10:48:08
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ToolForRegExp
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    messagedialog.cpp \
    datamanager.cpp \
    dataindialog.cpp

HEADERS  += widget.h \
    messagedialog.h \
    datamanager.h \
    dataindialog.h

FORMS    += widget.ui \
    messagedialog.ui \
    dataindialog.ui

RC_ICONS = R.ico

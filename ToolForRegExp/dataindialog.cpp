#include "dataindialog.h"
#include "ui_dataindialog.h"

DataInDialog::DataInDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataInDialog)
{
    ui->setupUi(this);
}

DataInDialog::~DataInDialog()
{
    delete ui;
}

QString DataInDialog::getRet()
{
    return m_strRet;
}

void DataInDialog::setRegExp(const QString &regExp)
{
    ui->textEditRegExp->setText(regExp);
}

void DataInDialog::on_pushButtonCancel_clicked(bool checked)
{
    m_strRet.clear();
    this->close();
}

void DataInDialog::on_pushButtonOk_clicked(bool checked)
{
    m_strRet.clear();
    m_strRet.append(ui->lineEditName->text());
    m_strRet.append(",");
    m_strRet.append(ui->textEditRegExp->toPlainText());

    this->close();
}

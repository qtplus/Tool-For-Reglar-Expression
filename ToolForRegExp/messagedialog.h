#ifndef MESSAGEDIALOG_H
#define MESSAGEDIALOG_H

#include <QDialog>
#include <QMap>
#include <QClipboard>
#include <QApplication>

namespace Ui {
class MessageDialog;
}

class MessageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MessageDialog(const QString &strId = "", QWidget *parent = 0);
    ~MessageDialog();

    void InitMessageMap();
    void ShowMessage();
    void SetRegExp(const QString &regExp);

private slots:
    void on_pushButtonClose_clicked(bool checked);
    void on_pushButtonCopyAndClose_clicked(bool checked);

private:
    Ui::MessageDialog *ui;
    QString                 m_strId;
    QMap<QString,QString>   m_mapMessage;
    QString                 m_strRegExp;
};

#endif // MESSAGEDIALOG_H

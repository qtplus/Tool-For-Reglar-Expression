#include "datamanager.h"

DataManager::DataManager(QObject *parent) : QObject(parent)
{
    if (!OpenDB())
    {
        QMessageBox::critical(0,"Error","Open Database Fail");
    }
    else
    {
        InitDB();
    }
}

DataManager::~DataManager()
{
    if (m_db.isOpen())
    {
        m_db.close();
    }
}

bool DataManager::OpenDB()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName("CustomRegExp.db");
    return m_db.open();
}

bool DataManager::InitDB()
{
    QSqlQuery query(m_db);
    QString l_strQuery;

    l_strQuery = "CREATE TABLE CustomRegExp("
                 "NAME           TEXT    NOT NULL,"
                 "REGEXP         BLOB    NOT NULL"
                 ");";
    return query.exec(l_strQuery);
}

bool DataManager::Query()
{
    QSqlQuery query(m_db);
    return query.exec(m_strQuery);
}

void DataManager::setQueryString(const QString &string)
{
    m_strQuery = string;
}

bool DataManager::getRowDatas(QStringList &stringList)
{
    QSqlQuery query(m_db);
    QString l_strQuery;
    l_strQuery = "select name,RegExp from CustomRegExp;";
    if (query.exec(l_strQuery))
    {
        while (query.next())
        {
            stringList.append(query.value(0).toString()+","+query.value(1).toString());
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool DataManager::getRowNames(QStringList &stringList)
{
    QSqlQuery query(m_db);
    QString l_strQuery;
    l_strQuery = "select name from CustomRegExp;";
    if (query.exec(l_strQuery))
    {
        while (query.next())
        {
            stringList.append(query.value(0).toString());
        }
        return true;
    }
    else
    {
        return false;
    }
}

QString DataManager::getRegExpByName(const QString &name)
{
    QSqlQuery query(m_db);
    QString l_strQuery;
    l_strQuery = "select RegExp from CustomRegExp where name='"+name+"';";
    if (query.exec(l_strQuery))
    {
        query.next();
        return query.value(0).toString();
    }
    else
    {
        return "";
    }
}

#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QtSql/QSqlRecord>
#include <QVariant>
#include <QDebug>

class DataManager : public QObject
{
    Q_OBJECT
public:
    explicit DataManager(QObject *parent = 0);
    ~DataManager();

    bool OpenDB();
    bool InitDB();
    bool Query();
    void setQueryString(const QString &string);
    bool getRowDatas(QStringList &stringList);
    bool getRowNames(QStringList &stringList);
    QString getRegExpByName(const QString &name);

signals:

public slots:

private:
    QSqlDatabase        m_db;
    QString             m_strQuery;
};

#endif // DATAMANAGER_H

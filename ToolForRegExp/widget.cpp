#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    InitListWidget();
    m_pDataManager = new DataManager(this);

    connect(ui->listWidget, SIGNAL(clicked(QModelIndex)), this, SLOT(slt_listWidget_clicked(QModelIndex)));
    ui->stackedWidget->setCurrentIndex(0);
    UpdateCustomRegExpListWidget();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::InitListWidget()
{
    m_strlstItemsId.clear();
    m_strlstItemsId
            << "匹配中文字符"
            << "匹配双字节字符(包括汉字在内)"
            << "匹配空白行"
            << "匹配Email地址"
            << "匹配网址URL"
            << "匹配国内电话号码"
            << "匹配腾讯QQ号"
            << "匹配中国邮政编码"
            << "匹配18位身份证号"
            << "匹配(年-月-日)格式日期"
            << "匹配正整数"
            << "匹配负整数"
            << "匹配整数"
            << "匹配非负整数(正整数+0)"
            << "匹配非正整数(负整数+0)"
            << "匹配正浮点数"
            << "匹配负浮点数";
    ui->listWidget->insertItems(ui->listWidget->count(),m_strlstItemsId);
    ui->listWidget->show();
}

void Widget::UpdateCustomRegExpListWidget()
{
    QStringList l_strlstItemsName;
    if (m_pDataManager->getRowNames(l_strlstItemsName))
    {
        ui->listWidgetCustom->clear();
        ui->listWidgetCustom->addItems(l_strlstItemsName);
    }
}

void Widget::on_pushButtonQuit_clicked(bool checked)
{
    this->close();
}

void Widget::on_comboBox_currentIndexChanged(int index)
{
    if (ui->stackedWidget->count() > index)
    {
        ui->stackedWidget->setCurrentIndex(index);

        if (ui->comboBox->currentText() == "在线正则表达式")
        {
            ui->textEditRegExp->setReadOnly(false);
            ui->lineEditTestText->setReadOnly(true);
            ui->lineEditTestText->setEnabled(false);
            ui->textEditRegExp->setFocus();
        }
        if (ui->comboBox->currentText() == "自定义正则表达式")
        {
            ui->pushButtonDelete->setEnabled(false);
        }
    }
    else
    {
        QMessageBox::critical(this,"Error","Unexpected error");
    }
}

void Widget::slt_listWidget_clicked(QModelIndex modelIndex)
{
    MessageDialog l_MessageDialog(m_strlstItemsId.value(modelIndex.row()));
    l_MessageDialog.ShowMessage();
}

void Widget::on_pushButtonReset_clicked(bool checked)
{
    ui->textEditRegExp->setReadOnly(false);
    ui->textEditRegExp->clear();
    ui->textEditRegExp->setEnabled(true);
    ui->lineEditTestText->setReadOnly(true);
    ui->lineEditTestText->clear();
    ui->lineEditTestText->setEnabled(false);
    ui->textEditRegExp->setFocus();
}

void Widget::on_pushButtonTest_clicked(bool checked)
{
    ui->textEditRegExp->setReadOnly(true);
    ui->textEditRegExp->setEnabled(false);
    ui->lineEditTestText->setReadOnly(false);
    ui->lineEditTestText->clear();
    ui->lineEditTestText->setEnabled(true);
    ui->lineEditTestText->setFocus();

    QRegExp l_QRegExp(ui->textEditRegExp->toPlainText());
    QRegExpValidator *l_pQRegExpVld = new QRegExpValidator(l_QRegExp,this);
    ui->lineEditTestText->setValidator(l_pQRegExpVld);
}

void Widget::on_pushButtonCopy_clicked(bool checked)
{
    QClipboard *l_pQClipboard = QApplication::clipboard();
    l_pQClipboard->setText(ui->textEditRegExp->toPlainText());
}

void Widget::on_pushButtonPaste_clicked(bool checked)
{
    ui->textEditRegExp->setReadOnly(false);
    ui->textEditRegExp->clear();
    ui->textEditRegExp->setEnabled(true);
    ui->lineEditTestText->setReadOnly(true);
    ui->lineEditTestText->clear();
    ui->lineEditTestText->setEnabled(false);

    QClipboard *l_pQClipboard = QApplication::clipboard();
    ui->textEditRegExp->clear();
    ui->textEditRegExp->setText(l_pQClipboard->text());

    ui->textEditRegExp->setFocus();
    QTextCursor l_QTextCursor = ui->textEditRegExp->textCursor();
    l_QTextCursor.movePosition(QTextCursor::End);
    ui->textEditRegExp->setTextCursor(l_QTextCursor);
}

void Widget::on_pushButtonAddToCustom_clicked(bool checked)
{
    if (ui->textEditRegExp->toPlainText() == "")
    {
        return;
    }
    DataInDialog l_dataInDialog;
    l_dataInDialog.setRegExp(ui->textEditRegExp->toPlainText());
    l_dataInDialog.exec();
    QString l_strRet = l_dataInDialog.getRet();
    if (l_strRet == "")
    {
        return;
    }
    QStringList l_strlstRet = l_strRet.split(",");
    if (l_strlstRet.count() == 2)
    {
        m_pDataManager->setQueryString("insert into CustomRegExp values ('"+l_strlstRet.value(0)
                                       +"','"+l_strlstRet.value(1)+"');");
        if (m_pDataManager->Query())
        {
            UpdateCustomRegExpListWidget();
        }
        else
        {
            QMessageBox::critical(0,"Error","Add Data Fail");
        }
    }
}

void Widget::on_pushButtonAdd_clicked(bool checked)
{
    DataInDialog l_dataInDialog;
    l_dataInDialog.exec();
    QString l_strRet = l_dataInDialog.getRet();
    if (l_strRet == "")
    {
        return;
    }
    QStringList l_strlstRet = l_strRet.split(",");
    if (l_strlstRet.count() == 2)
    {
        m_pDataManager->setQueryString("insert into CustomRegExp values ('"+l_strlstRet.value(0)
                                       +"','"+l_strlstRet.value(1)+"');");
        if (m_pDataManager->Query())
        {
            UpdateCustomRegExpListWidget();
        }
        else
        {
            QMessageBox::critical(0,"Error","Add Data Fail");
        }
    }
}

void Widget::on_pushButtonDelete_clicked(bool checked)
{
        m_pDataManager->setQueryString("delete from CustomRegExp where name='"
                                       +ui->listWidgetCustom->currentItem()->text()+"';");
        if (m_pDataManager->Query())
        {
            QMessageBox::critical(0,"Tips","Delete Data Success");
            UpdateCustomRegExpListWidget();
        }
        else
        {
            QMessageBox::critical(0,"Error","请选择要删除的数据");
        }
        ui->pushButtonDelete->setEnabled(false);
}

void Widget::on_listWidgetCustom_itemPressed(QListWidgetItem *item)
{
    ui->pushButtonDelete->setEnabled(true);
}

void Widget::on_listWidgetCustom_doubleClicked(const QModelIndex &index)
{
    MessageDialog l_MessageDialog;
    l_MessageDialog.SetRegExp(m_pDataManager->getRegExpByName(ui->listWidgetCustom->currentItem()->text()));
    l_MessageDialog.ShowMessage();
}

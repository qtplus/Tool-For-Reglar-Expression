#include "messagedialog.h"
#include "ui_messagedialog.h"

MessageDialog::MessageDialog(const QString &strId, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MessageDialog)
{
    ui->setupUi(this);
    m_strId = strId;

    InitMessageMap();
}

MessageDialog::~MessageDialog()
{
    delete ui;
}

void MessageDialog::InitMessageMap()
{
    m_mapMessage.clear();
    m_mapMessage.insert("匹配中文字符",
                        "[\\x4e00-\\x9fa5]");
    m_mapMessage.insert("匹配双字节字符(包括汉字在内)",
                        "[^\\x00-\\xff]");
    m_mapMessage.insert("匹配空白行",
                        "\\n\\s*\\r");
    m_mapMessage.insert("匹配Email地址",
                        "[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?");
    m_mapMessage.insert("匹配网址URL",
                        "[a-zA-z]+://[^\\s]*");
    m_mapMessage.insert("匹配国内电话号码",
                        "\\d{3,4}-\\d{7,8}");
    m_mapMessage.insert("匹配腾讯QQ号",
                        "[1-9][0-9]{4,9}");
    m_mapMessage.insert("匹配中国邮政编码",
                        "\\d{6}");
    m_mapMessage.insert("匹配18位身份证号",
                        "^(\\d{17})([0-9]|X)$");
    m_mapMessage.insert("匹配(年-月-日)格式日期",
                        "([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8])))");
    m_mapMessage.insert("匹配正整数",
                        "^[1-9]\\d*$");
    m_mapMessage.insert("匹配负整数",
                        "^-[1-9]\\d*$");
    m_mapMessage.insert("匹配整数",
                        "^-?[1-9]\\d*$");
    m_mapMessage.insert("匹配非负整数(正整数+0)",
                        "^[1-9]\\d*|0$");
    m_mapMessage.insert("匹配非正整数(负整数+0)",
                        "^-[1-9]\\d*|0$");
    m_mapMessage.insert("匹配正浮点数",
                        "^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*$");
    m_mapMessage.insert("匹配负浮点数",
                        "^-[1-9]\\d*\\.\\d*|-0\\.\\d*[1-9]\\d*$");
}

void MessageDialog::ShowMessage()
{
    ui->textEditMessage->clear();
    if (m_mapMessage.contains(m_strId))
    {
        ui->textEditMessage->setText(m_mapMessage.value(m_strId));
    }
    else
    {
        ui->textEditMessage->setText(m_strRegExp);
    }
    ui->textEditMessage->selectAll();

    this->exec();
}

void MessageDialog::SetRegExp(const QString &regExp)
{
    m_strRegExp = regExp;
}

void MessageDialog::on_pushButtonClose_clicked(bool checked)
{
    this->close();
}

void MessageDialog::on_pushButtonCopyAndClose_clicked(bool checked)
{
    QClipboard *l_pQClipboard = QApplication::clipboard();
    l_pQClipboard->setText(ui->textEditMessage->toPlainText());
    this->close();
}

#ifndef DATAINDIALOG_H
#define DATAINDIALOG_H

#include <QDialog>

namespace Ui {
class DataInDialog;
}

class DataInDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DataInDialog(QWidget *parent = 0);
    ~DataInDialog();

    QString getRet();
    void setRegExp(const QString &regExp);

private slots:
    void on_pushButtonCancel_clicked(bool checked);
    void on_pushButtonOk_clicked(bool checked);

private:
    Ui::DataInDialog *ui;
    QString             m_strRet;
};

#endif // DATAINDIALOG_H

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMessageBox>
#include <QStringList>
#include <QDebug>
#include <QModelIndex>
#include "messagedialog.h"
#include "datamanager.h"
#include "dataindialog.h"
#include <QListWidgetItem>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void InitListWidget();
    void UpdateCustomRegExpListWidget();

private slots:
    void on_pushButtonQuit_clicked(bool checked);
    void on_comboBox_currentIndexChanged(int index);
    void slt_listWidget_clicked(QModelIndex modelIndex);
    void on_pushButtonReset_clicked(bool checked);
    void on_pushButtonTest_clicked(bool checked);
    void on_pushButtonCopy_clicked(bool checked);
    void on_pushButtonPaste_clicked(bool checked);
    void on_pushButtonAddToCustom_clicked(bool checked);
    void on_pushButtonAdd_clicked(bool checked);
    void on_pushButtonDelete_clicked(bool checked);
    void on_listWidgetCustom_itemPressed(QListWidgetItem *item);
    void on_listWidgetCustom_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;
    QStringList         m_strlstItemsId;
    DataManager         *m_pDataManager;
};

#endif // WIDGET_H
